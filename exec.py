#!/usr/bin/env python3

import os
import tkinter as tk

from shutil import copyfile
from tkinter.filedialog import askopenfile
from tkinter.ttk import Combobox, Separator


NONE_VALUE = "--- NONE ---"
COUNTRIES = [
    NONE_VALUE,
    "Austria",
    "Belgium",
    "Bulgaria",
    "Croatia",
    "Cyprus",
    "Czechia",
    "Denmark",
    "Estonia",
    "Europe",
    "Finland",
    "France",
    "Germany",
    "Greece",
    "Hungary",
    "Ireland",
    "Italy",
    "Latvia",
    "Lithuania",
    "Luxembourg",
    "Malta",
    "Netherlands",
    "Poland",
    "Portugal",
    "Romania",
    "Slovakia",
    "Slovenia",
    "Spain",
    "Sweden",
    "United Kingdom",
]
PRESET_DICT = {}
PRESET_LIST = []


class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.__create_widgets_for_title()
        self.__create_widgets_for_subtitle()
        self.__create_widgets_for_countries()
        self.__create_widgets_for_preset()
        self.__create_buttons()

    def create_output_html(self):
        with open("index.html") as f:
            lines = [line.rstrip() for line in f]

        for i in range(1, len(lines)):
            lines[i] = lines[i].replace("${TITLE}", self.inp_title.get())
            lines[i] = lines[i].replace("${SUBTITLE}", self.inp_subtitle.get())

        with open("output.html", "w") as o:
            o.writelines("%s\n" % i for i in lines)

        try:
            os.remove("./img/current.png")
        except Exception:
            pass

        country = self.cmb_countries.get()
        if country and country != NONE_VALUE:
            country = country.replace(" ", "_").lower()
            img_path_from = f"./img/all/{country}.png"
            img_path_to = "./img/current.png"
            copyfile(img_path_from, img_path_to)

    def refresh(self):
        self.__remove_widgets_for_title()
        self.__create_widgets_for_title()

        self.__remove_widgets_for_subtitle()
        self.__create_widgets_for_subtitle()
        
        self.__remove_widgets_for_preset()
        self.__create_widgets_for_preset()

    def __create_widgets_for_title(self):
        self.lbl_title = tk.Label(self)
        self.lbl_title["text"] = "Title of the banner (e.g. name of speaker):"
        self.lbl_title.grid(row=10, column=0)

        self.inp_title = tk.Entry(self, bd=1, width=30)
        self.inp_title.grid(row=10, column=1)

    def __create_widgets_for_subtitle(self):
        self.lbl_subtitle = tk.Label(self)
        self.lbl_subtitle["text"] = "Subtitle of the banner:"
        self.lbl_subtitle.grid(row=20, column=0)

        self.inp_subtitle = tk.Entry(self, bd=1, width=30)
        self.inp_subtitle.grid(row=20, column=1)

    def __create_widgets_for_countries(self):
        self.lbl_counties = tk.Label(self)
        self.lbl_counties["text"] = "Countries"
        self.lbl_counties.grid(row=30, column=0)

        self.cmb_countries = Combobox(self, width=28)
        self.cmb_countries["values"] = COUNTRIES
        self.cmb_countries["state"] = "readonly"
        self.cmb_countries.grid(row=30, column=1)

    def __create_widgets_for_preset(self):
        self.lbl_preset = tk.Label(self)
        self.lbl_preset["text"] = "Choose from preset"
        self.lbl_preset.grid(row=40, column=0)

        self.cmb_preset = Combobox(self, width=28)
        self.cmb_preset["values"] = PRESET_LIST
        self.cmb_preset["state"] = "readonly"
        self.cmb_preset.grid(row=40, column=1)
        self.cmb_preset.bind(
            "<<ComboboxSelected>>",
            self.__handle_on_combobox_preset_changed
        )

    def __remove_widgets_for_title(self):
        self.inp_title.delete(0, tk.END)

    def __remove_widgets_for_subtitle(self):
        self.inp_subtitle.delete(0, tk.END)

    def __remove_widgets_for_preset(self):
        self.cmb_preset.delete(0, "end")

    def __create_buttons(self):
        self.btn_separator = Separator(self, orient=tk.HORIZONTAL)
        self.btn_separator.grid(row=99, column=0, columnspan=5, sticky="ew")

        self.btn_preset_file = tk.Button(self)
        self.btn_preset_file["text"] = "Open Preset File"
        self.btn_preset_file["command"] = self.__handle_on_button_preset_file
        self.btn_preset_file.grid(row=100, column=0)

        self.btn_ok = tk.Button(self)
        self.btn_ok["text"] = "Switch Banner"
        self.btn_ok["command"] = self.__handle_on_button_ok
        self.btn_ok.grid(row=100, column=1)

    def __handle_on_button_ok(self):
        self.create_output_html()

    def __handle_on_button_preset_file(self):
        file_to_open = askopenfile(mode="r")
        if file_to_open is not None:
            lines = file_to_open.readlines()
            for line in lines:
                line = line.split(";")
                title = line[0] if len(line) > 0 else ""
                subtitle = line[1] if len(line) > 1 else ""
                country = line[2] if len(line) > 2 else ""

                PRESET_LIST.append(title)
                PRESET_DICT[title] = (subtitle, country)
        self.refresh()

    def __handle_on_combobox_preset_changed(self, event):
        title = event.widget.get()
        entry = PRESET_DICT[title]
        self.refresh()
        self.inp_title.insert(0, title)
        self.inp_subtitle.insert(0, entry[0])
        self.cmb_countries.set(entry[1].rstrip())


window = tk.Tk()
window.title("Banner Switcher")
app = Application(master=window)
app.mainloop()
